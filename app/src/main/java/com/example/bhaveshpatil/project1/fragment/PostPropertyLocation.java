package com.example.bhaveshpatil.project1.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bhaveshpatil.project1.R;

public class PostPropertyLocation extends Fragment {

    TextView TV_sell,TV_rent,TV_pg;
    AutoCompleteTextView TV_city;
    EditText editText_address,editText_locality,editText_project,editText_coveredArea,editText_carpetArea,editText_price;
    Spinner spinner_propertyType;
    Button button_net;
    ImageView imageView_photos;

  //  public static String UPLOAD_URL="https://bhaveshpatil.000webhostapp.com/upload_loc.php";

    ImageButton addPhotos;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    String propertyType,city,coveredarea,carpetarea,address,locality,project,price;


    public PostPropertyLocation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       View view=inflater.inflate(R.layout.fragment_post_property_location,container,false);

        TV_city=view.findViewById(R.id.TV_city);

        editText_address=view.findViewById(R.id.editText_address);
        editText_locality=view.findViewById(R.id.editText_locality);
        editText_project=view.findViewById(R.id.editText_project);
        editText_coveredArea=view.findViewById(R.id.editText_coveredArea);
        editText_carpetArea=view.findViewById(R.id.edittext_carpetArea);
        editText_price=view.findViewById(R.id.edittext_price);

        spinner_propertyType=view.findViewById(R.id.spinner_propertyType);

        button_net=view.findViewById(R.id.button_net);

        imageView_photos=view.findViewById(R.id.imageView_photos);
        addPhotos=view.findViewById(R.id.addPhotos);


        final String [] prop_type={"Select Property Type","Flat","House","Villa","Office Space","Pent House","shop"};
        ArrayAdapter<String> arrayAdapter1= new ArrayAdapter(getActivity(),R.layout.spinner_item,R.id.textView_item, prop_type);
        spinner_propertyType.setAdapter(arrayAdapter1);


        button_net.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 address=editText_address.getText().toString();
                 locality=editText_locality.getText().toString();
                 project=editText_project.getText().toString();
                 carpetarea=editText_carpetArea.getText().toString();
                 coveredarea=editText_coveredArea.getText().toString();
                 price=editText_price.getText().toString();
                 propertyType=spinner_propertyType.getSelectedItem().toString();
                 city=TV_city.getText().toString();

              if (address.isEmpty() || locality.isEmpty() || project.isEmpty() || carpetarea.isEmpty() ||  coveredarea.isEmpty() || price.isEmpty() || propertyType.isEmpty() || city.isEmpty()) {
                    Toast.makeText(getActivity(), "fill details", Toast.LENGTH_SHORT).show();

                }else {

                    SharedPreferences sharedPreferences= getActivity().getSharedPreferences("DETAILS",Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit=sharedPreferences.edit();
                    edit.putString("propertyType",propertyType);
                    edit.putString("coveredArea",coveredarea);
                    edit.putString("carpetArea",carpetarea);
                    edit.putString("city",city);
                    edit.putString("locality",locality);
                    edit.putString("project",project);
                    edit.putString("address",address);
                    edit.putString("price",price);
                    edit.apply();


                    Toast.makeText(getActivity(), "done", Toast.LENGTH_SHORT).show();

                    loadFragment();

               }

            }
        });

        addPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                permission();

            }
        });


        return view;
    }
/*
    private void upload() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
        String sellerType = sharedPreferences.getString("sellerType","");
        String  dealType= sharedPreferences.getString("dealType","");



          try{
            String uploadid= UUID.randomUUID().toString();
            new MultipartUploadRequest(getActivity(),uploadid,UPLOAD_URL)
                    .addParameter("seller_type",sellerType)
                    .addParameter("dealType",dealType)
                    .addParameter("property_type",propertyType)
                    .addParameter("covered_area",coveredarea)
                    .addParameter("carpet_area",carpetarea)
                    .addParameter("city",city)
                    .addParameter("locality",locality )
                    .addParameter("project",project)
                    .addParameter("address",address)
                    .addParameter("price",price)
                    .setMaxRetries(2)
                    .startUpload();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }

*/

    private void permission() {

        if(ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(getActivity(),new String[]{android.Manifest.permission.CAMERA},123);

        }
        else {
            uploadPhoto();
        }

    }

    private void uploadPhoto() {

        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
      //  if (intent.resolveActivity(getPackageManager()) != null) {
        //    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        //}
        }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 123) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                uploadPhoto();
            } else {
                Toast.makeText(getActivity(), "Deny", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void loadFragment() {

        PostPropertyDetails fragment = new PostPropertyDetails();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }



}
