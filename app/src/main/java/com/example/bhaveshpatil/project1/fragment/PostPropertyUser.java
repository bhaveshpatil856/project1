package com.example.bhaveshpatil.project1.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.bhaveshpatil.project1.R;


public class PostPropertyUser extends Fragment {


    RadioGroup RG_sellerType;
    RadioButton radioButton_owner,radioButton_agent,radioButton_builder;

    Button button_nxt;



    public PostPropertyUser() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

           View view= inflater.inflate(R.layout.fragment_post_property_user,container,false);



        RG_sellerType=view.findViewById(R.id.RG_sellerType);
        radioButton_agent=view.findViewById(R.id.radioButton_agent);
        radioButton_builder=view.findViewById(R.id.radioButton_builder);
        radioButton_owner=view.findViewById(R.id.radioButton_owner);

        button_nxt=view.findViewById(R.id.button_nxt);




        button_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sellerType=RG_sellerType.getCheckedRadioButtonId();
                RadioButton seller=getView().findViewById(sellerType);

                if (RG_sellerType.getCheckedRadioButtonId()== -1) {
                    Toast.makeText(getActivity(), "Select Option", Toast.LENGTH_SHORT).show();
                }       else {

                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("sellerType", seller.getText().toString());
                    edit.commit();

                    loadFragment();
                }
// Toast.makeText(getActivity(),seller.getText().toString(),Toast.LENGTH_LONG).show();

                }
        });

        return view;
    }

    private void loadFragment() {

        PostPropertyType fragment = new PostPropertyType();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }


}
