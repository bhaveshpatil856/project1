package com.example.bhaveshpatil.project1.model;

public class PropertyListRepo {

    String id;
    String project;
    String city;
    String locality;
    String address;
    String price;
    String number;


    public PropertyListRepo() {

        this.id= id;
        this.project = project;
        this.city = city;
        this.locality = locality;
        this.address = address;
        this.price = price;
        this.number = number;

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }



}
