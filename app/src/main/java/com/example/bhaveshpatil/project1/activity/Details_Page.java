package com.example.bhaveshpatil.project1.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bhaveshpatil.project1.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Details_Page extends AppCompatActivity {

    TextView TV_project,TV_address,TV_price,TV_desc,textView_coveredArea,textView_carpetArea,textView_bedrooms,textView_bathrooms,textView_balcony,textView_furnishing,textView_floor,textView_possesion,textView_amenities,textView_parking,textView_condition;
//    private List<PropertyDetailsRepo> details;
    String id,name,sellerType,dealType,propertyType,coveredArea,carpetArea,city,locality,project,address,price,bedrooms,bathrooms,balcony,totalFloor,floorNo,furnishing,status,propertyCondition,Amenities,parking,propertyDesc,number,email;
 //   private PropertyDetailsRepo[] data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details__page);

         id=getIntent().getStringExtra("id");

         cast();



        displayDetails(id);

    //    Toast.makeText(this, id, Toast.LENGTH_SHORT).show();


    }

    private void cast() {

        TV_project=findViewById(R.id.TV_project);
        TV_address=findViewById(R.id.TV_address);
        TV_price=findViewById(R.id.TV_price);
        TV_desc=findViewById(R.id.TV_desc);
        textView_coveredArea=findViewById(R.id.textView_coveredArea);
        textView_carpetArea=findViewById(R.id.textView_carpetArea);
        textView_bedrooms=findViewById(R.id.textView_bedrooms);
        textView_bathrooms=findViewById(R.id.textView_bathrooms);
        textView_balcony=findViewById(R.id.textView_balcony);
        textView_furnishing=findViewById(R.id.textView_furnishing);
        textView_floor=findViewById(R.id.textView_floor);
        textView_possesion=findViewById(R.id.textView_possesion);
        textView_parking=findViewById(R.id.textView_parking);
        textView_condition=findViewById(R.id.textView_codition);
        textView_amenities=findViewById(R.id.textView_amenities);




    }

    private void displayDetails(final String id) {


        final String path="https://bhaveshpatil.000webhostapp.com/displayDetails.php";

        StringRequest stringRequest=new StringRequest(Request.Method.POST, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.v("DATA",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for(int i=0;i < jsonArray.length();i++)
                    {
                        JSONObject property =jsonArray.getJSONObject(i);
                       // PropertyDetailsRepo detailsRepo = new PropertyDetailsRepo();
                       String id= property.getString("id").trim();
                        name = property.getString("name").trim();
                        sellerType = property.getString("sellerType").trim();
                        dealType = property.getString("dealType").trim();
                        propertyType = property.getString("propertyType").trim();
                        coveredArea = property.getString("coveredArea").trim();
                        carpetArea = property.getString("carpetArea").trim();
                        city = property.getString("city").trim();
                        locality = property.getString("locality").trim();
                        project = property.getString("project").trim();
                        address = property.getString("address").trim();
                        price = property.getString("price").trim();
                        bedrooms = property.getString("bedroom").trim();
                        bathrooms = property.getString("bathroom").trim();
                        balcony = property.getString("balcony").trim();
                        totalFloor = property.getString("totalFloor").trim();
                        floorNo = property.getString("floorNo").trim();
                        furnishing = property.getString("furnishing").trim();
                        status = property.getString("status").trim();
                        Amenities = property.getString("Amenities").trim();
                        parking = property.getString("Parking").trim();
                        propertyCondition = property.getString("PropertyCondition").trim();
                        propertyDesc = property.getString("propertyDesc").trim();
                        number = property.getString("number").trim();
                        email = property.getString("email").trim();
                        //details.add(detailsRepo);



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

               setdata();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "opps!", Toast.LENGTH_SHORT).show();

            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> stringMap = new HashMap<String, String>();
                stringMap.put("id",id);
                return stringMap;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    @SuppressLint("SetTextI18n")
    private void setdata() {

        TV_project.setText(project);
        TV_address.setText(address + "," + locality + "\n" + city);
        TV_price.setText(price);
        textView_coveredArea.setText(coveredArea);
        textView_carpetArea.setText(carpetArea);
        textView_bedrooms.setText(bedrooms);
        textView_bathrooms.setText(bathrooms);
        textView_balcony.setText(balcony);
        textView_furnishing.setText(furnishing);
        textView_floor.setText(floorNo + "out of" + totalFloor);
        textView_possesion.setText(status);
        textView_parking.setText(parking);
        textView_condition.setText(propertyCondition);
        textView_amenities.setText(Amenities);
        TV_desc.setText(propertyDesc);


    }


}

