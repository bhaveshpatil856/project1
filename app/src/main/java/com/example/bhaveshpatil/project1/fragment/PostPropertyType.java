package com.example.bhaveshpatil.project1.fragment;



import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.bhaveshpatil.project1.R;
/**
 * A simple {@link Fragment} subclass.
 */
public class PostPropertyType extends Fragment {

    RadioGroup RG_dealType;
    RadioButton radioButton_sell, radioButton_rent, radioButton_pg;
    Button button_next;

    public PostPropertyType() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_post_property_type, container, false);


        RG_dealType = view.findViewById(R.id.RG_dealType);
        radioButton_sell = view.findViewById(R.id.radioButton_sell);
        radioButton_rent = view.findViewById(R.id.radioButton_rent);
        radioButton_pg = view.findViewById(R.id.radioButton_pg);

        button_next = view.findViewById(R.id.button_next);


        button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int dealType = RG_dealType.getCheckedRadioButtonId();
                RadioButton deal = getView().findViewById(dealType);

                if (RG_dealType.getCheckedRadioButtonId()== -1)
                {
                    Toast.makeText(getActivity(),"select option",Toast.LENGTH_SHORT).show();
                }
else {
                    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("dealType", deal.getText().toString());
                    edit.commit();

                    loadFragment();

                }
            }
        });

        return view;
    }

    private void loadFragment() {

        PostPropertyLocation fragment = new PostPropertyLocation();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


}