package com.example.bhaveshpatil.project1;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bhaveshpatil.project1.activity.HomePage;
import com.example.bhaveshpatil.project1.activity.LoginPage;
import com.example.bhaveshpatil.project1.activity.RegisterPage;

public class MainActivity extends AppCompatActivity {

    TextView textView_skip;
    Button button_signup,button_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button_login=findViewById(R.id.button_login);
        button_signup=findViewById(R.id.button_signup);
        textView_skip=findViewById(R.id.textView_skip);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), LoginPage.class));
            }
        });

        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), RegisterPage.class));

            }
        });

        textView_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(getApplicationContext(), FilterPage.class));
            }
        });

    }


}
