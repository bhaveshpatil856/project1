package com.example.bhaveshpatil.project1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.bhaveshpatil.project1.activity.HomePage;

public class FilterPage extends AppCompatActivity {

    Button button_set,button_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_page);

        button_clear=findViewById(R.id.button_clear);
        button_set=findViewById(R.id.button_set);

        button_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), HomePage.class));

            }
        });

        button_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(FilterPage.this, "Cleared", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
