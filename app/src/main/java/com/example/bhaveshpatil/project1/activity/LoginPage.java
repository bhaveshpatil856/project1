package com.example.bhaveshpatil.project1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bhaveshpatil.project1.R;

import java.util.HashMap;
import java.util.Map;

public class LoginPage extends AppCompatActivity {

    TextView textView_back,textView_login,textView_forgetPass,textView_register;
    EditText editText_username,editText_password;
    ImageView imageView_profile;

    public static String LURL ="https://bhaveshpatil.000webhostapp.com/login.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        textView_login=findViewById(R.id.textView_login);
        textView_back=findViewById(R.id.textView_back);
        textView_forgetPass=findViewById(R.id.textView_forgetPass);
        textView_register=findViewById(R.id.textView_register);

        editText_username=findViewById(R.id.editText_username);
        editText_password=findViewById(R.id.editText_password);

        imageView_profile=findViewById(R.id.imageView_profile);

        textView_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),RegisterPage.class));

            }
        });

        textView_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String user=editText_username.getText().toString();
                final String pass=editText_password.getText().toString();

                StringRequest stringRequest=new StringRequest(Request.Method.POST, LURL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("success"))
                        {
                            Toast.makeText(getApplicationContext(), "Login Successfull", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),HomePage.class));
                        }

                        if (response.contains("invalid user"))
                        {
                            Toast.makeText(getApplicationContext(), "Invalid User! try again", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),LoginPage.class));
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> stringMap = new HashMap<>();

                        stringMap.put("uemail",user);

                        stringMap.put("upass",pass);

                        return stringMap;

                    }
                };

                RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                stringRequest.setShouldCache(false);
                requestQueue.add(stringRequest);




            }
        });


        textView_forgetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(),"nantar karu",Toast.LENGTH_LONG).show();
            }
        });

        textView_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(),HomePage.class));
            }
        });
    }
}
