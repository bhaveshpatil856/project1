package com.example.bhaveshpatil.project1.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;


import com.example.bhaveshpatil.project1.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import net.gotev.uploadservice.MultipartUploadRequest;

import java.net.MalformedURLException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class Confirmation extends Fragment {

    FirebaseAuth auth;
    EditText editText_otp;
    TextView textView_resend;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    Button button_verify, button_finish;
    private String verificationCode;
    String code;
    String number;

    public static String UPLOAD_URL = "https://bhaveshpatil.000webhostapp.com/uploadProperty.php";


    public Confirmation() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirmation, container, false);

        editText_otp = view.findViewById(R.id.editText_otp);
        textView_resend = view.findViewById(    R.id.textView_resend);
        button_finish = view.findViewById(R.id.button_finish);
        button_verify = view.findViewById(R.id.button_verify);

        StartFirebaseLogin();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
        number = sharedPreferences.getString("number", "");
        Toast.makeText(getActivity(), number, Toast.LENGTH_SHORT).show();

        sendVerificationCode(number);


        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code =editText_otp.getText().toString();
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, code);
                SigninWithPhone(credential);

            }


        });


        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upload();
            }
        });


        return view;

    }


    private void upload() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);

        String sellerType = sharedPreferences.getString("sellerType", "");
        String dealType = sharedPreferences.getString("dealType", "");
        String propertyType = sharedPreferences.getString("propertyType", "");
        String coveredArea = sharedPreferences.getString("coveredArea", "");
        String carpetArea = sharedPreferences.getString("carpetArea", "");
        String city = sharedPreferences.getString("city", "");
        String locality = sharedPreferences.getString("locality", "");
        String project = sharedPreferences.getString("project", "");
        String address = sharedPreferences.getString("address", "");
        String price = sharedPreferences.getString("price", "");
        String bedroom = sharedPreferences.getString("bedroom", "");
        String bathroom = sharedPreferences.getString("bathroom", "");
        String balcony = sharedPreferences.getString("balcony", "");
        String totalFloor = sharedPreferences.getString("totalFloor", "");
        String floorNo = sharedPreferences.getString("floorNo", "");
        String Furnishing = sharedPreferences.getString("furnishing", "");
        String Status = sharedPreferences.getString("status", "");
        String Amenities = sharedPreferences.getString("Amenities", "");
        String Parking = sharedPreferences.getString("Parking", "");
        String condition = sharedPreferences.getString("condition", "");
        String propertyDesc = sharedPreferences.getString("propertyDesc", "");
        String number = sharedPreferences.getString("number", "");
        String email = sharedPreferences.getString("email", "");
        String name = sharedPreferences.getString("name", "");

        Toast.makeText(getActivity(), Amenities, Toast.LENGTH_LONG).show();

        try {
            String uploadid = UUID.randomUUID().toString();

            new MultipartUploadRequest(getActivity(), uploadid, UPLOAD_URL)
                    .addParameter("name", name)
                    .addParameter("sellerType", sellerType)
                    .addParameter("dealType", dealType)
                    .addParameter("propertyType", propertyType)
                    .addParameter("coveredArea", coveredArea)
                    .addParameter("carpetArea", carpetArea)
                    .addParameter("city", city)
                    .addParameter("locality", locality)
                    .addParameter("project", project)
                    .addParameter("address", address)
                    .addParameter("price", price)
                    .addParameter("bedroom", bedroom)
                    .addParameter("bathroom", bathroom)
                    .addParameter("balcony", balcony)
                    .addParameter("totalFloor", totalFloor)
                    .addParameter("floorNo", floorNo)
                    .addParameter("furnishing", Furnishing)
                    .addParameter("status", Status)
                    .addParameter("Amenities", Amenities)
                    .addParameter("Parking", Parking)
                    .addParameter("PropertyCondition", condition)
                    .addParameter("propertyDesc", propertyDesc)
                    .addParameter("number", number)
                    .addParameter("email", email)
                    .setMaxRetries(2)
                    .startUpload();

            //Toast.makeText(getActivity(), uploadid,Toast.LENGTH_LONG).show();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }


    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
               "+91" + number,                     // Phone number to verify
                60,                           // Timeout duration
                TimeUnit.SECONDS,                // Unit of timeout
                getActivity(),      // Activity (for callback binding)
                mCallback);                      // OnVerificationStateChangedCallbacks

    }

    private void StartFirebaseLogin() {
        auth = FirebaseAuth.getInstance();
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(getActivity(),"verification completed",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(getActivity(),e.toString(),Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                Toast.makeText(getActivity(),"Code sent",Toast.LENGTH_SHORT).show();
            }
        };
    }


    private void SigninWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getActivity(), "Verified no.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(),"Incorrect OTP",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}