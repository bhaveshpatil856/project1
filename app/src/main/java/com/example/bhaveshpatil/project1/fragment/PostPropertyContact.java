package com.example.bhaveshpatil.project1.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bhaveshpatil.project1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostPropertyContact extends Fragment {

    EditText editText_email,editText_contact,editText_name;
    Button button_generateOtp;
    String number;


    public PostPropertyContact() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_post_property_contact, container, false);

        editText_email=view.findViewById(R.id.editText_email);
        editText_contact=view.findViewById(R.id.editText_contact);
        editText_name=view.findViewById(R.id.editText_name);

        button_generateOtp=view.findViewById(R.id.button_generateOtp);

        button_generateOtp.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {


             number = editText_contact.getText().toString().trim();
             String email = editText_email.getText().toString();
             String name = editText_name.getText().toString();

             if (number.isEmpty() || email.isEmpty()||name.isEmpty()) {
                 Toast.makeText(getActivity(), "fill details", Toast.LENGTH_SHORT).show();
             } else {
                 SharedPreferences sharedPreferences = getActivity().getSharedPreferences("DETAILS", Context.MODE_PRIVATE);
                 SharedPreferences.Editor edit = sharedPreferences.edit();
                 edit.putString("email", email);
                 edit.putString("number", number);
                 edit.putString("name",name);
                 edit.apply();

                 loadFragment();
             }
         }

         });






        return view;

    }




    private void loadFragment() {

        Confirmation fragment = new Confirmation();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }


}
