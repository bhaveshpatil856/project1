package com.example.bhaveshpatil.project1.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bhaveshpatil.project1.R;
import com.example.bhaveshpatil.project1.adapter.newPropertyAdapter;
import com.example.bhaveshpatil.project1.fragment.PostPropertyUser;
import com.example.bhaveshpatil.project1.model.PropertyListRepo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomePage extends AppCompatActivity {

    private BottomNavigationView navigation_bar;
    RecyclerView recycler_new,recycler_hotDeals;
    private ArrayList<PropertyListRepo> propertyList;
    newPropertyAdapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        navigation_bar=findViewById(R.id.navigation_bar);

        recycler_new=findViewById(R.id.recycler_new);
        recycler_hotDeals=findViewById(R.id.recycler_hotDeals);

        propertyList= new ArrayList<>();

        showproperty();




        navigation_bar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);



    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
             Fragment fragment;
            switch (item.getItemId()) {
                case R.id.nav_home:
                    startActivity(new Intent(getApplicationContext(),HomePage.class));
                    return true;
                case R.id.nav_chat:
                    startActivity(new Intent(getApplicationContext(), List.class));
                    return true;
                case R.id.nav_addProperty:
                    fragment=new PostPropertyUser();
                    loadFragments(fragment);

                    return true;
                case R.id.nav_notification:
                    startActivity(new Intent(getApplicationContext(), ProfilePage.class));
                    return true;
                case R.id.nav_menu:
                    startActivity(new Intent(getApplicationContext(), ProfilePage.class));
                    return true;
            }
            return false;
        }
    };

    private void loadFragments(Fragment fragment) {

        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showproperty() {

        String PATH="https://bhaveshpatil.000webhostapp.com/displayProperty.php";

        StringRequest stringRequest=new StringRequest(Request.Method.POST, PATH, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.v("DATA",response);
                try {

                    // Log.i("tagconvertstr", "["+response+"]");
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    for(int i=0;i < jsonArray.length();i++)
                    {
                        JSONObject Property =jsonArray.getJSONObject(i);
                        PropertyListRepo propertyListRepo = new PropertyListRepo();
                        propertyListRepo.setId(Property.getString("id"));
                        propertyListRepo.setProject(Property.getString("project"));
                        propertyListRepo.setCity(Property.getString("city"));
                        propertyListRepo.setLocality(Property.getString("locality"));
                        propertyListRepo.setAddress(Property.getString("address"));
                        propertyListRepo.setPrice(Property.getString("price"));
                        propertyListRepo.setNumber(Property.getString("number"));
                        propertyList.add(propertyListRepo);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                adapter = new newPropertyAdapter(HomePage.this,propertyList);
                LinearLayoutManager layoutManager= new LinearLayoutManager(HomePage.this,LinearLayoutManager.HORIZONTAL, false);
                recycler_new.setLayoutManager(layoutManager);
                recycler_new.setHasFixedSize(true);
                recycler_new.setAdapter(adapter);




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();


            }

        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);

    }



}
